<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardRequest;
use App\Models\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{
    public function set_card_info(Request $request)
    {
        $rules = [
            'card_number' => ["required", "numeric"],
            'hesab_number' => ["required", "numeric"],
            'cvv2' => ["required", "numeric"],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::find(1);
        $profile = $user->profile;
        $profile->update([
            'shaba_number' => $request->shaba_number,
            'card_number' => $request->card_number,
            'hesab_number' => $request->hesab_number,
            'expiration_date' => $request->expiration_date,
            'cvv2' => $request->cvv2,
        ]);
        return response()->json(['profile'=>$profile]);
    }
}
