<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class TransactionController extends Controller
{

    public function save_apply($request, $user_id)
    {
        $apply = new Apply();
        $apply->amount = $request->amount;
        $apply->description = $request->description;
        $apply->destinationFirstname = $request->destinationFirstname;
        $apply->destinationLastname = $request->destinationLastname;
        $apply->destinationNumber = $request->destinationNumber;
        $apply->paymentNumber = $request->paymentNumber;
        $apply->deposit = $request->deposit;
        $apply->sourceFirstName = $request->sourceFirstName;
        $apply->sourceLastName = $request->sourceLastName;
        $apply->reasonDescription = $request->reasonDescription;
        $apply->user_id = $user_id;
        $apply->save();
        return $apply;
    }

    public function save_done_transaction($response, $apply_id, $user_id)
    {
        $transaction = new Transaction();
        $transaction->amount = $response['result']['amount'];
        $transaction->description = $response['result']['description'];
        $transaction->destinationFirstname = $response['result']['destinationFirstname'];
        $transaction->destinationLastname = $response['result']['destinationLastname'];
        $transaction->inquiryDate = $response['result']['inquiryDate'];
        $transaction->inquirySequence = $response['result']['inquirySequence'];
        $transaction->destinationNumber = $response['result']['destinationNumber'];
        $transaction->inquiryTime = $response['result']['inquiryTime'];
        $transaction->message = $response['result']['message'];
        $transaction->paymentNumber = $response['result']['paymentNumber'];
        $transaction->refCode = $response['result']['refCode'];
        $transaction->sourceFirstname = $response['result']['sourceFirstname'];
        $transaction->sourceLastname = $response['result']['sourceLastname'];
        $transaction->sourceNumber = $response['result']['sourceNumber'];
        $transaction->type = $response['result']['type'];
        $transaction->reasonDescription = $response['result']['reasonDescription'];
        $transaction->status = $response['status'];
        $transaction->trackId = $response['trackId'];
        $transaction->apply_id = $apply_id;
        $transaction->user_id = $user_id;
        $transaction->save();
        return $transaction;
    }

    public function save_failed_transaction($response, $apply_id, $user_id)
    {
        $transaction = new Transaction();
        $transaction->status = $response['status'];
        $transaction->trackId = $response['trackId'];
        $transaction->code = $response['error']['code'];
        $transaction->message = $response['error']['message'];
        $transaction->apply_id = $apply_id;
        $transaction->user_id = $user_id;
        $transaction->save();
        return $transaction;
    }


    public function apply_transaction(Request $request)
    {
        $user = User::find(1);
        $apply = $this->save_apply($request, $user->id);
        $response = Http::withHeaders([
            'content-type' => 'application/json',
            'Authorization' => 'Bearer TOKEN',
        ])->post('https://apibeta.finnotech.ir/oak/v2/clients/{clientId}/transferTo?trackId={trackId}', [
            "amount" => $request->amount,
            "description" => $request->description,
            "destinationFirstname" => $request->destinationFirstname,
            "destinationLastname" => $request->destinationLastname,
            "destinationNumber" => $request->destinationNumber,
            "paymentNumber" => $request->paymentNumber,
            "deposit" => $request->deposit,
            "sourceFirstName" => $request->sourceFirstName,
            "sourceLastName" => $request->sourceLastName,
            "reasonDescription" => $request->reasonDescription
        ]);
        if ($response->status() == 200) {
            $transaction = $this->save_done_transaction($response, $apply->id, $user->id);
            return response($transaction);
        } elseif ($response->status() == 400) {
            $transaction = $this->save_failed_transaction($response, $apply->id, $user->id);
            return response($transaction);
        }
    }

    public function get_transactions(){
        $user = User::find(1);
        $transactions = Transaction::where(['user_id'=>$user->id])->orderBy('id','DESC')->get();
        return response()->json(['transactions'=>$transactions]);
    }

}
