<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Symfony\Component\Translation\t;

class Transaction extends Model
{
    use HasFactory;

    public function apply(){
        return $this->hasOne(Apply::class);
    }
}
