<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount',15,2);
            $table->string('description');
            $table->string('destinationFirstname');
            $table->string('destinationLastname');
            $table->string('destinationNumber');
            $table->string('inquiryDate');
            $table->string('inquirySequence');
            $table->string('inquiryTime');
            $table->string('message');
            $table->string('paymentNumber');
            $table->string('refCode');
            $table->string('sourceFirstname');
            $table->string('sourceLastname');
            $table->string('sourceNumber');
            $table->enum('type',['internal','paya']);
            $table->string('reasonDescription');
            $table->enum('status',['DONE','FAILED']);
            $table->string('trackId');
            $table->unsignedBigInteger('apply_id');
            $table->unsignedBigInteger('user_id');
            $table->string('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
