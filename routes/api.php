<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('customer')->group(function () {
    Route::post('/set-card-info',[App\Http\Controllers\Api\Customer\CardController::class,'set_card_info']);
    Route::post('/apply-transaction',[App\Http\Controllers\Api\Customer\TransactionController::class,'apply_transaction']);
    Route::get('/get-transactions',[App\Http\Controllers\Api\Customer\TransactionController::class,'get_transactions']);
});
